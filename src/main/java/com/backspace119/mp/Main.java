package com.backspace119.mp;

import javax.sound.midi.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


public class Main {
    public static final int NOTE_ON = 0x90;
    public static final int NOTE_OFF = 0x80;
    public static final String[] NOTE_NAMES = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
    private static final byte SET_TEMPO = 81;
    public static int[] voicePointers = {0, 0, 0, 0, 0, 0};
    public static short[][] voicesAndBeats = new short[12][8000];
    public static double[] lastBeatOnVoice = {0, 0, 0, 0, 0, 0};
    public static int voicePointer = 0;
    public static int startVoice = 0;
    public static boolean[] voiceFree = {true, true, true, true, true, true};
    public static int newStartVoice = 0;
    public static int tempo = 0;
    public static int[] lastNoteOnVoice = {0, 0, 0, 0, 0, 0};
    public static int[] lastOctaveOnVoice = {0, 0, 0, 0, 0, 0};
    public static double smallestNote = 32 * 15;
    private static Map<Integer,Integer> currentPatch = new HashMap<>();
    private static Map<Integer,Integer> voiceOnChannel = new HashMap<>();
    public static void main(String[] args) {

        Sequence sequence = null;
        try {
            sequence = MidiSystem.getSequence(new File(args[0]));
        } catch (InvalidMidiDataException | IOException e) {
            e.printStackTrace();
            return;
        }
        boolean bpmBasedTime = false;
        float divisionType = sequence.getDivisionType();
        double tickTimeMs = 0;
        if (divisionType == Sequence.PPQ) {
            bpmBasedTime = true;

        } else {
            tickTimeMs = sequence.getResolution() * divisionType;
            tickTimeMs /= 1000;
            tickTimeMs = 1.0 / tickTimeMs;
        }
        List<String> output = new ArrayList<>();
        int trackNumber = 0;
        for (Track track : sequence.getTracks()) {

            trackNumber++;
            output.add("Track " + trackNumber + ": size = " + track.size());
            startVoice = newStartVoice;
            voicePointer = startVoice;
            System.out.println("Track: " + trackNumber + " Start voice: " + newStartVoice);
            for (int i = 0; i < lastNoteOnVoice.length; i++) {
                lastNoteOnVoice[i] = 0;
                lastOctaveOnVoice[i] = 0;
                lastBeatOnVoice[i] = 0;
            }
            for (int i = 0; i < voicePointer; i += 2) {
                voiceFree[i / 2] = false;
            }
            for (int i = 0; i < track.size(); i++) {
                StringBuilder sb = new StringBuilder();
                MidiEvent event = track.get(i);
                //TODO we are not supporting partial beats, which are most certainly a thing.
                double eventBeat = 0;
                if (bpmBasedTime) {
                    sb.append("@").append((double) (((double) event.getTick() * smallestNote) / (double) sequence.getResolution())).append(" 32nd notes ");
                    eventBeat = (double) (((double) event.getTick() * smallestNote) / (double) sequence.getResolution());
                } else {
                    sb.append("@").append(event.getTick() * tickTimeMs).append("ms ");
                    eventBeat = ((event.getTick() * tickTimeMs) / (tempo / 60_000));
                }
                MidiMessage message = event.getMessage();
                if (message instanceof ShortMessage) {
                    ShortMessage sm = (ShortMessage) message;
                    if(sm.getCommand() == ShortMessage.PROGRAM_CHANGE)
                    {
                        System.out.println("Channel: " + sm.getChannel() + " Program change to: " + sm.getData1());
                        currentPatch.put(sm.getChannel(),sm.getData1());
                    }
                    sb.append("Channel: ").append(sm.getChannel()).append(" ");
                    if (sm.getCommand() == NOTE_ON && currentPatch.get(sm.getChannel()) != 25) {
                        int key = sm.getData1();
                        int octave = (key / 12) - 1;
                        int note = key % 12;
                        String noteName = NOTE_NAMES[note];
                        int velocity = sm.getData2();
                        sb.append("Note on, ").append(noteName).append(octave).append(" key=").append(key).append(" velocity: ").append(velocity);

                        if (velocity == 0) {
                            doNoteOff(sm, eventBeat);

                        } else {
                            try {
                                errorOnNoVoices();
                            } catch (OutOfVoicesError e) {
                                System.out.println("Error on note: " + NOTE_NAMES[note] + "" + octave + " at: " + eventBeat + "");
                                //e.printStackTrace();
                                output.add(sb.toString());
                                continue;
                            }

                            voicePointer = firstFreeVoice();
                            voiceFree[voicePointer / 2] = false;
                            if(voicePointer == newStartVoice)
                            newStartVoice = voicePointer + 2;
                            double delta = (eventBeat - lastBeatOnVoice[voicePointer / 2]);
                            if (delta != 0) {
                                while (delta > 255) {
                                    delta -= 255;
                                    addRestToVoice(255);
                                }
                                addRestToVoice(delta);
                                lastBeatOnVoice[voicePointer / 2] = eventBeat;
                            }


                            //System.out.println("Note at: " + eventBeat + " started " + NOTE_NAMES[note] + "" + octave);
                            addNoteStartToVoice(note, octave);
                            lastNoteOnVoice[voicePointer / 2] = note;
                            lastOctaveOnVoice[voicePointer / 2] = octave;
                        }

                    } else if (sm.getCommand() == NOTE_OFF) {
                        doNoteOff(sm, eventBeat);
                        int key = sm.getData1();
                        int octave = (key / 12) - 1;
                        int note = key % 12;
                        String noteName = NOTE_NAMES[note];
                        int velocity = sm.getData2();
                        sb.append("Note off, ").append(noteName).append(octave).append(" key=").append(key).append(" velocity: ").append(velocity);

                    } else {
                        sb.append("Command:").append(sm.getCommand());
                    }
                } else if (message instanceof MetaMessage) {
                    MetaMessage mm = (MetaMessage) message;
                    if (mm.getType() == SET_TEMPO) {
                        //only accept first tempo, we do not have tempo change setup yet on the arduino
                        if (tempo == 0) {
                            tempo = (mm.getData()[0] & 0xFF) << 16 | (mm.getData()[1] & 0xFF) << 8 | mm.getData()[2] & 0xFF;
                            System.out.println("Tempo: " + tempo);
                            tempo = 60_000_000 / tempo;
                            System.out.println("Set bpm to: " + tempo);
                            sb.append("Set bpm to: ").append(tempo);
                        }
                    } else {
                        sb.append(Integer.toHexString(mm.getType()));
                    }
                } else {

                    sb.append("Other message: ").append(message.getClass());
                }
                output.add(sb.toString());
            }


        }
        System.out.print("PROGMEM const byte line1[] =  ");
        short[] out = new short[voicePointers[0]];
        System.arraycopy(voicesAndBeats[0], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line1_beats[] =  ");
        System.arraycopy(voicesAndBeats[1], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line2[] =  ");
        out = new short[voicePointers[1]];
        System.arraycopy(voicesAndBeats[2], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line2_beats[] =  ");
        System.arraycopy(voicesAndBeats[3], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line3[] =  ");
        out = new short[voicePointers[2]];
        System.arraycopy(voicesAndBeats[4], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line3_beats[] =  ");
        System.arraycopy(voicesAndBeats[5], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");

        System.out.print("PROGMEM const byte line4[] =  ");
        out = new short[voicePointers[3]];
        System.arraycopy(voicesAndBeats[6], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line4_beats[] =  ");
        System.arraycopy(voicesAndBeats[7], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line5[] =  ");
        out = new short[voicePointers[4]];
        System.arraycopy(voicesAndBeats[8], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line5_beats[] =  ");
        System.arraycopy(voicesAndBeats[9], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line6[] =  ");
        out = new short[voicePointers[5]];
        System.arraycopy(voicesAndBeats[10], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");
        System.out.print("PROGMEM const byte line6_beats[] =  ");
        System.arraycopy(voicesAndBeats[11], 0, out, 0, out.length);
        System.out.println(Arrays.toString(out).replaceAll("\\[","{").replaceAll("]","}") + ";");

        try {
            PrintWriter pw = new PrintWriter(new FileWriter(new File("output.txt")));
            for (String line : output)
                pw.println(line);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void doNoteOff(ShortMessage sm, double eventBeat) {
        int key = sm.getData1();
        int octave = (key / 12) - 1;
        int note = key % 12;
        int p = 0;
        for (int lastNote : lastNoteOnVoice) {
            if (lastNote == note) {
                if (lastOctaveOnVoice[p / 2] == octave) {
                    //System.out.println("Note at: " + eventBeat + " ended " + NOTE_NAMES[note] + "" + octave);
                    int carryPointer = voicePointer;
                    voicePointer = p;
                    double delta = (eventBeat - lastBeatOnVoice[voicePointer / 2]);
                    if (delta == 0) {
                        voicePointer = carryPointer;
                        continue;
                    }
                    setNoteLength(delta);
                    lastBeatOnVoice[voicePointer / 2] = eventBeat;
                    voiceFree[voicePointer / 2] = true;


                    break;
                }
            }
            p += 2;
        }
    }

    public static int firstFreeVoice() {
        int i = 0;
        for (boolean b : voiceFree) {
            if (b) return i;
            i += 2;
        }
        return -1;
    }

    public static boolean checkVoices() {
        for (boolean b : voiceFree) {
            if (b) return true;
        }
        return false;
    }

    public static void errorOnNoVoices() {

        if (!checkVoices()) {
            throw new OutOfVoicesError("Over 6 simultaneous notes played, only 6 voices available, cannot parse file");
        }
    }

    public static boolean checkSpace() {
        return voicePointers[voicePointer / 2] < voicesAndBeats[voicePointer].length;
    }

    public static void errorIfOutOfSpace() {
        if (!checkSpace()) {
            throw new OverflowError("Voice" + voicePointer + " out of space");
        }
    }

    public static void addRestToVoice(double beats) {
        if (beats > 255) {
            System.out.println("Rest longer than 255 beats: " + voicePointer + ":" + voicePointers[voicePointer / 2]);
        }
        errorIfOutOfSpace();
        voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]] = 0;
        voicesAndBeats[voicePointer + 1][voicePointers[voicePointer / 2]] = (short) beats;
        if (voicesAndBeats[voicePointer + 1][voicePointers[voicePointer / 2]] > 255)
            throw new OverflowError("Beats cast to over 255 when storing: " + voicePointer + ":" + voicePointers[voicePointer / 2] + " beats as double: " + beats);
        voicePointers[voicePointer / 2]++;

    }

    public static void addNoteStartToVoice(int note, int octave) {
        //System.out.println("Adding note: " + NOTE_NAMES[note] + "" + octave + " to voice: " + voicePointer);
        errorIfOutOfSpace();
        note++;//our format has 0 as rest, where notes start at 1
        short noteAndOctave = (short) ((0x0F & note) | ((octave << 4) & 0xF0));
        voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]] = noteAndOctave;

    }

    public static void addExtendNote(short note) {
        //System.out.println("Value before extend: " + voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]]);
        //System.out.println("Received value: " + note);
        voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]] = (short) (note | 0x80);
        //System.out.println("Value after extend: " + voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]]);
    }

    public static void addNote(short note) {
        voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]] = (short)(note & ~0x80);
    }

    public static void setNoteLength(double beats) {
        if (beats == 0) {
            System.out.println("Note has zero beats: " + voicePointer + ":" + voicePointers[voicePointer / 2]);
            beats++;
        }
        if (beats > 255) {
            System.out.println("Extending note: " + voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]]);
            addExtendNote(voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]]);
            while (beats > 255) {

                //System.out.println("Extending note: " + voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]]);
                errorIfOutOfSpace();
                voicesAndBeats[voicePointer + 1][voicePointers[voicePointer / 2]] = 255;
                addExtendNote(voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]]);
                voicePointers[voicePointer / 2]++;
                beats -= 255;
            }
            addNote(voicesAndBeats[voicePointer][voicePointers[voicePointer / 2]]);
            errorIfOutOfSpace();
            voicesAndBeats[voicePointer + 1][voicePointers[voicePointer / 2]] = (short) beats;
            if (voicesAndBeats[voicePointer + 1][voicePointers[voicePointer / 2]] > 255)
                throw new OverflowError("Beats cast to over 255 when storing: " + voicePointer + ":" + voicePointers[voicePointer / 2] + " beats as double: " + beats);
            voicePointers[voicePointer / 2]++;

        } else {
            errorIfOutOfSpace();
            voicesAndBeats[voicePointer + 1][voicePointers[voicePointer / 2]] = (short) beats;
            if (voicesAndBeats[voicePointer + 1][voicePointers[voicePointer / 2]] > 255)
                throw new OverflowError("Beats cast to over 255 when storing: " + voicePointer + ":" + voicePointers[voicePointer / 2] + " beats as double: " + beats);
            voicePointers[voicePointer / 2]++;
        }
        //System.out.println("Note off on voice: " + voicePointer);

    }
}
