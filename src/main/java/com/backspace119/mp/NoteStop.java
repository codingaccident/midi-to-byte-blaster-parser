package com.backspace119.mp;

public class NoteStop {

    private int note = 0;
    private int octave = 0;
    private double eventBeat = 0;

    public NoteStop(int note, int octave, int eventBeat)
    {
        this.note = note;
        this.octave = octave;
        this.eventBeat = eventBeat;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public int getOctave() {
        return octave;
    }

    public void setOctave(int octave) {
        this.octave = octave;
    }

    public double getEventBeat() {
        return eventBeat;
    }

    public void setEventBeat(double eventBeat) {
        this.eventBeat = eventBeat;
    }
}
