package com.backspace119.mp;

public class OverflowError extends Error {
    public OverflowError(String msg)
    {
        super(msg);
    }
}
