package com.backspace119.mp;

public class OutOfVoicesError extends Error {

    public OutOfVoicesError(String msg)
    {
        super(msg);
    }
}
