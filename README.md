This program converts midi files to a format the Byte Blaster sound card can read.

The Byte Blaster sound card is a card designed to run on a 65XX based system that has an SPI bus. It's based off the Atmega328p and does 6 total voices, which can be 3L3R or 6M

The card is a work in progress, and so the format that it reads may change, so I will not go into detail just yet about what the format is exactly. This program is also a work in progress, and contains some bugs that make some files not play nicely with the sound card.

This project is licensed under the GPL v3 license.